FROM ubuntu:latest
RUN apt-get update
RUN apt-get install --no-install-recommends -y python3 python3-pip
RUN rm -fr /var/lib/apt/lists/*
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY app.py /opt/
ENTRYPOINT FLASK_APP=/opt/app.py flask run --host=0.0.0.0 --port=8080
